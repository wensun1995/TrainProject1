﻿using System;
using System.Web;
using System.Linq;
using WebApplication1.Models;
using System.Collections.Generic;
using WebApplication1.DataAccessLayer;
using System.Web.Mvc;

namespace WebApplication1.BusinessLayer
{
    public class EmployeeBusinessLayer
    {
        public List<Employee> GetEmployees()
        {
            SalesERPDAL testDal = new SalesERPDAL();
            return testDal.Employees.ToList();
        }

        public Employee SaveEmployee(Employee e)
        {
            SalesERPDAL testDal = new SalesERPDAL();
            testDal.Employees.Add(e);
            testDal.SaveChanges();
            return e;
        }

        public UserStatus GetUserValidity(UserDetails u)
        {
            if (u.UserName == "admin" && u.Password == "admin")
            {
                return UserStatus.AuthenticatedAdmin;
            }
            else if (u.UserName == "sun" && u.Password == "sun")
            {
                return UserStatus.AuthentucatedUser;
            }
            else
            {
                return UserStatus.NonAuthenticatedUser;
            }
        }

        /*public bool IsValidUser(UserDetails u)
        {
            if (u.UserName == "admin" && u.Password == "admin")
            {
                return true;
            }
            else
            {
                return false;
            }
        }*/

        public void UploadEmployees(List<Employee> employees)
        {
            SalesERPDAL salesDal = new SalesERPDAL();
            salesDal.Employees.AddRange(employees);
            salesDal.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            Exception e = new Exception("Invalid Controller or/and Active Name");
            HandleErrorInfo eInfo = new HandleErrorInfo(e, "Unknow", "Unknow");
            return View("Error",eInfo);
        }
    }
}
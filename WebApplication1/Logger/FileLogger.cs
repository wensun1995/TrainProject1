﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication1.Logger
{
    public class FileLogger
    {
        public void LogException(Exception e)
        {
            File.WriteAllLines("E://Error//" +
                DateTime.Now.ToString("dd-mm-yyyy hh-mm-ss")
                + ".txt",
                new string[]
                {
                    "Message:"+e.Message,
                    "StackTrace:"+e.StackTrace
                });
        }
    }
}